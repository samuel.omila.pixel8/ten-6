<?php
    // Connect to the database using MySQLi
    $host = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'employee_db';


    $conn = new mysqli($host, $username, $password, $database);


    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Insert data into the employee table
    $sql = "INSERT INTO employee (`first_name`, `last_name`, `middle_name`, `birthday`, `address`)
            VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully<br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }


    // retrieve the first name, last name, and birthday of all employees in the table
    $sql =  "SELECT * FROM employee";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        echo "<br>Employees<br>";
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }

    // Write a SQL query to retrieve the number of employees whose last name starts with the letter 'D''.

    $sql =  "SELECT * FROM employee WHERE last_name LIKE 'D%'";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        echo "<br>Employee that last_name start with letter D<br>";
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }


    //  Write a SQL query to retrieve the first name, last name, and address of the employee with the highest ID number.
    $sql =  "SELECT first_name, last_name, address
            FROM employee
            WHERE id = (SELECT MAX(id) FROM employee)";
    $result = $conn->query($sql);


    if ($result->num_rows > 0) {
        echo "<br>Employee employee with the highest ID number<br>";
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
        }
    } else {
        echo "0 results";
    }

    // Write a SQL query to update the address of the employee with the first name 'John' to '123 Main Street'.
    $sql = "UPDATE `employee` SET `address` = '123 Main Street' WHERE `first_name` = 'John'";
    if ($conn->query($sql) === TRUE) {
        echo "<br>Record updated successfully<br>";
    } else {
        echo "<br>Error updating record: " . $conn->error . "<br>";
    }


    // Write a SQL query to delete all employees whose last name starts with the letter 'D'.
    $sql = "DELETE FROM `employee` WHERE `last_name` LIKE 'D%'";
    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    $conn->close();

?>
